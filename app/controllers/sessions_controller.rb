class SessionsController < ApplicationController
  protect_from_forgery with: :exception
  include SessionsHelper
  def new
  end

  def create
    user = User.find_by_email(params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
        flash.now[:success] = "Logged in"
        log_in user
        redirect_back_or user
    else
      flash.now[:danger] = "Invalid email/password combination"
      render 'new'
    end

  end

  def destroy
    log_out
    redirect_to root_path
  end
end
